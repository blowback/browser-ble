const cfgServiceUuid = "5f6d4f53-5f43-4647-5f53-56435f49445f"; //_mOS_CFG_SVC_ID__
const cfgKeyUuid =     "306d4f53-5f43-4647-5f6b-65795f5f5f30"; // 0mOS_CFG_key___0
const cfgValueUuid =   "316d4f53-5f43-4647-5f76-616c75655f31"; // 1mOS_CFG_value_1
const cfgSaveUuid =    "326d4f53-5f43-4647-5f73-6176655f5f32"; // 2mOS_CFG_save__2

const rpcServiceUuid = "5f6d4f53-5f52-5043-5f53-56435f49445f"; // _mOS_RPC_SVC_ID_
const rpcDataUuid =    "5f6d4f53-5f52-5043-5f64-6174615f5f5f"; // (_mOS_RPC_data___)
const rpcTxCtlUuid =   "5f6d4f53-5f52-5043-5f74-785f63746c5f"; // (_mOS_RPC_tx_ctl_)
const rpcRxCtlUuid =   "5f6d4f53-5f52-5043-5f72-785f63746c5f"; // (_mOS_RPC_rx_ctl_)

const RPC_MTU = 512; // can we read this from the API? spec says 512 is doable

class BLE {

    static generate_id() {
        let id = Math.random() * Number.MAX_SAFE_INTEGER;
        return id.toFixed();
    }

    async connect() {
        try {
            this.device = await navigator.bluetooth.requestDevice({
                filters: [
                    //{
                    //services: [
                    //cfgServiceUuid,
                    //rpcServiceUuid
                    //]
                    //}
                    {
                        namePrefix: 'UTTO'
                    }
                ],
                optionalServices: [ cfgServiceUuid, rpcServiceUuid ]
            });
            console.log("BLE device:", this.device.name);

            this.server = await this.device.gatt.connect();
            console.log("GATT server: ", this.server);

            this.rpcService = await this.server.getPrimaryService(rpcServiceUuid);
            console.log("RPC service: ", this.rpcService);

            this.rpcTxCtl = await this.rpcService.getCharacteristic(rpcTxCtlUuid);
            console.log("RPC TX CTL characteristic: ", this.rpcTxCtl);

            this.rpcData = await this.rpcService.getCharacteristic(rpcDataUuid);
            console.log("RPC DATA characteristic: ", this.rpcData);

            this.rpcRxCtl = await this.rpcService.getCharacteristic(rpcRxCtlUuid);
            console.log("RPC RX CTL characteristic: ", this.rpcRxCtl);

            this.cfgService = await this.server.getPrimaryService(cfgServiceUuid);
            console.log("CFG service: ", this.cfgService);

            this.cfgKey = await this.cfgService.getCharacteristic(cfgKeyUuid);
            console.log("CFG KEY characteristic: ", this.cfgKey);

            this.cfgValue = await this.cfgService.getCharacteristic(cfgValueUuid);
            console.log("CFG VALUE characteristic: ", this.cfgValue);

            this.cfgSave = await this.cfgService.getCharacteristic(cfgSaveUuid);
            console.log("CFG SAVE characteristic: ", this.cfgSave);
        } catch(err) {
            console.log(err);
        }
    }

    async writeChunked(characteristic, v) {
        if(typeof(v) === 'string') {
            var frame = new TextEncoder().encode(v);
        } else if(v instanceof Uint8Array) {
            var frame = v;
        } else {
            throw new Error("rats");
        }

        for(let sent = 0, to_send = 0; sent < frame.length; sent += to_send) {
            let remaining = frame.length - sent;
            to_send = (remaining < RPC_MTU) ? remaining : RPC_MTU;
            let chunk = new DataView(frame.buffer, sent, to_send);
            await characteristic.writeValue(chunk);
        }
    }

    async readChunked(characteristic, n) {
        if(n > 0) {
            let j = 0;
            let r = '';

            while(j < n) {
                let chunk = await characteristic.readValue();
                r += new TextDecoder('utf-8').decode(chunk);
                j += chunk.byteLength;
            }
            return r;
        }
    }

    async readUnchunked(characteristic) {
        let chunk = await characteristic.readValue();
        return new TextDecoder('utf-8').decode(chunk);
    }

    async getConfig(key) {
        console.log("KEY:", key);
        await this.writeChunked(this.cfgKey, key);
        let response = await this.readUnchunked(this.cfgValue);
        console.log("RESPONSE: ", response);
        return response;
    }

    async setConfig(key, value, saveType) {
        switch(saveType) {
            case 0:
                var save = '0';
            break;

            case 1:
                var save = '1';
            break;

            case 2:
                var save = '2';
            break;

            default:

                throw new Error("unrecognised save type");
            break;
        }

        await this.writeChunked(this.cfgKey, key);
        await this.writeChunked(this.cfgValue, value);
        await this.writeChunked(this.cfgSave, save);
        return true;
    }

    async callRPC(method, params = {}) {

        if(typeof(params) === 'string') {
            params = JSON.parse(params);
        }

        let id = BLE.generate_id();

        try {
            let call = { method, params, id }; // JSON-RPC 2.0

            let frame = new TextEncoder().encode(JSON.stringify(call));
            let n = frame.length;
            let b = new Uint8Array([(n >> 24) & 0xff, (n >> 16) & 0xff, (n >> 8) & 0xff, n & 0xff]);

            await this.rpcTxCtl.writeValue(b); // write frame length
            await this.writeChunked(this.rpcData, frame);

            for(let i = 0; i < 20; i++) {
                let rx_bytes = await this.rpcRxCtl.readValue();
                let u = new Uint8Array(rx_bytes.buffer);
                let n  = (u[0] << 24) | (u[1] << 16) | (u[2] << 8) | u[3];

                let response = await this.readChunked(this.rpcData, n);
                return response;
            }

        } catch(err) {
            console.log(err);
        }
    }
}


async function startButtonHandler(evt) {
    evt.target.classList.add('stripes');

    const ble = new BLE();
    await ble.connect();

    evt.target.classList.add('device-hidden');
    evt.target.classList.remove('stripes');

    const ui = document.querySelector('#device');
    ui.classList.remove('device-hidden');

    const rpc_button = document.querySelector('#rpc-send');

    rpc_button.addEventListener('pointerup', async evt2 => {
        const methodField = document.querySelector('#method');
        const paramsField = document.querySelector('#params');

        let method = methodField.value;
        let params = paramsField.value;

        if(params.length === 0) {
            params = null;
        }

        console.log(`callButtonHandler: method ${method}, params ${params}`);

        let response = await ble.callRPC(method, params);
        console.log("RESPONSE: ", response);

        const responseElem = document.querySelector('#rpc-result');
        responseElem.innerText = response;
    });

    const cfg_get_button = document.querySelector('#cfg-get');
    const cfg_set_button = document.querySelector('#cfg-set');

    cfg_get_button.addEventListener('pointerup', async evt2 => {
        const keyField = document.querySelector('#key');
        const valueField = document.querySelector('#value');

        let key = keyField.value;

        let response = await ble.getConfig(key);
        valueField.value = response;
    });

    cfg_set_button.addEventListener('pointerup', async evt2 => {
        const keyField = document.querySelector('#key');
        const valueField = document.querySelector('#value');

        let key = keyField.value;
        let value = valueField.value;

        let response = await ble.setConfig(key, value, 2);

        valueField.value = "Wrote OK";
    });
}



async function main() {
    const button = document.querySelector('#start-button');

    // BLE not active unless instigated by UI gesture
    button.addEventListener('pointerup', startButtonHandler);
}

document.addEventListener('DOMContentLoaded', async evt => {
    await main();
});
